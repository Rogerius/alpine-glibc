FROM alpine:3.6

RUN ALPINE_GLIBC_PACKAGE_VERSION="2.25-r0" &&\
    ALPINE_GLIBC_BASE_URL="https://github.com/sgerrand/alpine-pkg-glibc/releases/download" &&\
    \
    apk add --no-cache --virtual=.build-dependencies wget ca-certificates &&\
    \
    wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://raw.githubusercontent.com/sgerrand/alpine-pkg-glibc/master/sgerrand.rsa.pub &&\
    wget $ALPINE_GLIBC_BASE_URL/$ALPINE_GLIBC_PACKAGE_VERSION/glibc-$ALPINE_GLIBC_PACKAGE_VERSION.apk &&\
    wget $ALPINE_GLIBC_BASE_URL/$ALPINE_GLIBC_PACKAGE_VERSION/glibc-bin-$ALPINE_GLIBC_PACKAGE_VERSION.apk &&\
    wget $ALPINE_GLIBC_BASE_URL/$ALPINE_GLIBC_PACKAGE_VERSION/glibc-i18n-$ALPINE_GLIBC_PACKAGE_VERSION.apk &&\
    \
    apk add --no-cache \
        glibc-$ALPINE_GLIBC_PACKAGE_VERSION.apk \
        glibc-bin-$ALPINE_GLIBC_PACKAGE_VERSION.apk \
        glibc-i18n-$ALPINE_GLIBC_PACKAGE_VERSION.apk &&\
    \
    rm "/etc/apk/keys/sgerrand.rsa.pub" &&\
    \
    /usr/glibc-compat/bin/localedef --force --inputfile POSIX --charmap UTF-8 C.UTF-8 || true &&\
    echo "export LANG=C.UTF-8" > /etc/profile.d/locale.sh &&\
    \
    apk del glibc-i18n &&\
    \
    rm "/root/.wget-hsts" &&\
    apk del .build-dependencies &&\
    rm \
        glibc-$ALPINE_GLIBC_PACKAGE_VERSION.apk \
        glibc-bin-$ALPINE_GLIBC_PACKAGE_VERSION.apk \
        glibc-i18n-$ALPINE_GLIBC_PACKAGE_VERSION.apk

ENV LANG=C.UTF-8
